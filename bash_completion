# bash_completion: custom bash completion scripts
# Author: Sudaraka Wijesinghe <sudaraka@sudaraka.org>
# License: CC-BY
#

# Autocompletion for `cdp` (cd $PROJECTSDIR) function
_cdp() {
    local cur prev opts
    COMPREPLY=()

    PROJECTSDIR_BASE=`basename $PROJECTSDIR`

    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    opts=`find $PROJECTSDIR/ -maxdepth 1 -type d ! -name "$PROJECTSDIR_BASE" ! -name .stfolder -printf '%f\n'`

    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
}

complete -F _cdp cdp


# Autocompletion for `pyve` function
_pyve() {
    local cur prev opts
    COMPREPLY=()

    DIR_BASE=`basename $PYVEDIR`

    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    opts=`find $PYVEDIR/ -maxdepth 1 -type d ! -name "$DIR_BASE" -printf '%f\n'`

    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
}

complete -F _pyve pyve


# Make GIT completion work with "g" alias
# Based on Stackoverflow answer by chris_sutter and Ondrej Machulda
# https://stackoverflow.com/questions/342969/how-do-i-get-bash-completion-to-work-with-aliases
_FILE=/usr/share/git/completion/git-completion.bash
if [ -f $_FILE ]; then
  source $_FILE
  __git_complete g __git_main

fi


# Enable pnpm completion for `pnpm`, `npm` and `n` alias
_FILE=$HOME/.local/share/bash-completion/completions/pnpm

if [ -f $_FILE ]; then
  source $_FILE

  complete -F _pnpm_completion npm
  complete -F _pnpm_completion n
fi

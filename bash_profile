#
# ~/.bash_profile
#

set -o history

EXTRA_PATH=''

# Local/user bin directories
LOCAL_BIN="$HOME/.local/bin"
PATH=$LOCAL_BIN:$PATH

# Node.js and npm paths
PNPM_PREFIX="$HOME/.pnpm-modules"
export NODE_PATH="$PNPM_PREFIX/pnpm-global/3/node_modules"
EXTRA_PATH="$EXTRA_PATH $PNPM_PREFIX/bin"

# Rust & Cargo path
EXTRA_PATH="$EXTRA_PATH $HOME/.cargo/bin"

# Explicitly defined paths
for dir in $EXTRA_PATH; do
  [ ! -r $dir ] && continue

  if [ -d $dir ]; then
      PATH=$PATH:$dir
  fi;
done;

# Find all direct subdirectories of OPT_DIR
OPT_DIR="$HOME/opt"

for dir in $(find $OPT_DIR/* -maxdepth 0 -type d); do
  [ ! -r $dir ] && continue

  # If there is ./bin subdirectory, use it
  if [ -d $dir/bin ]; then
    dir=$dir/bin
  fi

  # Make sure there are executables in the chosen directory
  if [ ! -z "$(find $dir/* -maxdepth 0 -type f -executable)" ]; then
    # Make sure it's not already in $PATH
    if [ "${PATH#*$dir}" == "$PATH" ]; then
      PATH=$PATH:$dir
    fi
  fi
done

# Add `node_modules/.bin` directory under current directory to $PATH
# NOTE: this **MUST** be the last $PATH segment, so that binaries in there will
# only be used as the last resort.
PATH=$PATH:node_modules/.bin

export PATH

[[ -f ~/.bashrc ]] && . ~/.bashrc

if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
   exec startx
fi

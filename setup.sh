#!/bin/sh
#
# setup.sh: install configuration files into current user's environment.
# Author: Sudaraka Wijesinghe
#

echo 'Dotfiles Setup.'
echo '==============='
echo 'Copyright 2013-2019 Sudaraka Wijesinghe. <sudaraka@sudaraka.org>'
echo 'Creative Commons Attribution 3.0 Unported License.'
echo

DOTFILES_DIR=$(realpath $(dirname $0));

if [ -z "$DOTFILES_DIR" ]; then
  echo 'Unable to determine dotfiles repository.';
  echo 'Check if "realpath" package is missing in your system.'
  exit 1;
fi;

# Vim configuration {{{

echo 'Setup Vim Configuration'
echo

# Remove existing configuration and recreate directories
rm -fr ~/.vimrc 2>/dev/null
rm -fr ~/.vim 2>/dev/null

ln -sv "$DOTFILES_DIR/vim" ~/.vim

# Install Vundle from github
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Make vim use aspell dictionary
mkdir -pv ~/.vim/spell
rm ~/.vim/spell/en.utf-8.add
ln -sv ../../.aspell.en.pws ~/.vim/spell/en.utf-8.add

# Run vim command to install bundles
vim +PlugUpgrade +PlugClean +PlugUpdate +PlugInstall +CocUpdate +qa

echo

# }}}

# Bash configuration {{{

echo 'Setup Bash Configuration'
echo

# Remove existing configuration
rm -f ~/.bash_completion 2>/dev/null
rm -f ~/.bash_profile 2>/dev/null
rm -f ~/.bashrc 2>/dev/null
rm -f ~/.bashrc.local 2>/dev/null
rm -f ~/.signature 2>/dev/null

ln -sv "$DOTFILES_DIR/bash_completion" ~/.bash_completion
ln -sv "$DOTFILES_DIR/bash_profile" ~/.bash_profile
ln -sv "$DOTFILES_DIR/bashrc" ~/.bashrc
if [ -f $DOTFILES_DIR/bashrc.`uname -n` ]; then
   ln -sv "$DOTFILES_DIR/bashrc.`uname -n`" ~/.bashrc.local
fi;

ln -sv "$DOTFILES_DIR/signature" ~/.signature

echo

# }}}

# Git configuration {{{

echo 'Setup Git Configuration'
echo

# Remove existing configuration
if [ -L ~/.config/git ]; then
  rm -f ~/.config/git 2>/dev/null
else
  rm -fr ~/.config/git/ 2>/dev/null
fi

ln -sv "$DOTFILES_DIR/git" ~/.config/git

echo

# }}}

# GNU Screen configuration {{{

echo 'Setup GNU Screen Configuration'
echo

# Remove existing configuration
rm -f ~/.profiles 2>/dev/null
rm -f ~/.screenrc 2>/dev/null

ln -sv "$DOTFILES_DIR/profiles" ~/.profiles
ln -sv "$DOTFILES_DIR/screenrc" ~/.screenrc

echo

# }}}

# GUI configuration {{{

echo 'Setup GUI Configuration'
echo

# Remove existing configuration
rm -f ~/.xinitrc 2>/dev/null
rm -f ~/.Xresources 2>/dev/null
rm -f ~/.Xmodmap 2>/dev/null
rm -f ~/.gtkrc-2.0 2>/dev/null
rm -fr ~/.config/gtk-3.0 2>/dev/null
rm -f ~/.config/Trolltech.conf 2>/dev/null
rm -f ~/.gtk-bookmarks 2>/dev/null
rm -f ~/gtk-3.0/bookmarks 2>/dev/null
rm -fr ~/.config/i3 2>/dev/null
rm -f ~/.config/dunst/dunstrc 2>/dev/null
rm -f ~/.config/tilda/config_0 2>/dev/null

mkdir -p ~/.config/{dunst,tilda,gtk-3.0} >/dev/null 2>&1

ln -sv "$DOTFILES_DIR/xinitrc" ~/.xinitrc
ln -sv "$DOTFILES_DIR/Xresources" ~/.Xresources
ln -sv "$DOTFILES_DIR/gtkrc-2.0" ~/.gtkrc-2.0
ln -sv "$DOTFILES_DIR/Trolltech.conf" ~/.config/
ln -sv "$DOTFILES_DIR/gtkrc-3.0" ~/.config/gtk-3.0/settings.ini
ln -sv "$DOTFILES_DIR/gtk-bookmarks" ~/.gtk-bookmarks
ln -sv "$DOTFILES_DIR/gtk-bookmarks" ~/.config/gtk-3.0/bookmarks

if [ -f "$DOTFILES_DIR/Xmodmap.`uname -n`" ]; then
  ln -sv "$DOTFILES_DIR/Xmodmap.`uname -n`" ~/.Xmodmap
else
  ln -sv "$DOTFILES_DIR/Xmodmap" ~/.Xmodmap
fi

# setup i3wm config and scripts
PRIMARY_DISPLAY=`xrandr|grep '\sconnected'|cut -d' ' -f1|head -n1`;
SECONDARY_DISPLAY=`xrandr|grep 'HDMI'|cut -d' ' -f1|head -n1`;
WIRELESS_INTERFACE=`ip link|grep ': wl'|cut -d':' -f2|tr -d ' '`;
ETHERNET_INTERFACE=`ip link|grep ': en'|cut -d':' -f2|tr -d ' '`;
BATTERY_ID=`find /sys/class/power_supply -type l -name BAT*|xargs basename`
FONT_SIZE=11.5

# case "`xrandr |grep $PRIMARY_DISPLAY|cut -d' ' -f4|cut -d'x' -f1`" in
#   1920) FONT_SIZE=12;;
# esac

if [ -z "$WIRELESS_INTERFACE" ]; then
  WIRELESS_INTERFACE="wlnx0"
fi;

for file in config conkyrc; do
  rm $DOTFILES_DIR/i3/$file #>/dev/null 2>&1
  cp -v $DOTFILES_DIR/i3/$file{.default,} #>/dev/null 2>&1

  sed "s/%PRIMARY_DISPLAY%/$PRIMARY_DISPLAY/" \
    -i "$DOTFILES_DIR/i3/$file" \
    >/dev/null 2>&1;

  sed "s/%SECONDARY_DISPLAY%/$SECONDARY_DISPLAY/" \
    -i "$DOTFILES_DIR/i3/$file" \
    >/dev/null 2>&1;

  sed "s/%VERTICAL_DISPLAY%/$PRIMARY_DISPLAY/" \
    -i "$DOTFILES_DIR/i3/$file" \
    >/dev/null 2>&1;

  sed "s/%WIRELESS_INTERFACE%/$WIRELESS_INTERFACE/" \
    -i "$DOTFILES_DIR/i3/$file" \
    >/dev/null 2>&1;

  sed "s/%ETHERNET_INTERFACE%/$ETHERNET_INTERFACE/" \
    -i "$DOTFILES_DIR/i3/$file" \
    >/dev/null 2>&1;

  sed "s/%BATTERY_ID%/$BATTERY_ID/" \
    -i "$DOTFILES_DIR/i3/$file" \
    >/dev/null 2>&1;

  sed "s/%FONT_SIZE%/$FONT_SIZE/" \
    -i "$DOTFILES_DIR/i3/$file" \
    >/dev/null 2>&1;
done;

ln -sv "$DOTFILES_DIR/i3" ~/.config
ln -sv "$DOTFILES_DIR/i3/dunstrc" ~/.config/dunst
ln -sv "$DOTFILES_DIR/i3/tilda" ~/.config/tilda/config_0

mkdir -p ~/.local/bin >/dev/null 2>&1
rm ~/.local/bin/i3exit >/dev/null 2>&1
ln -sv "$DOTFILES_DIR/i3/i3exit" ~/.local/bin/i3exit

echo

# }}}

# Dictionary file (aspell) {{{

echo 'Setup Dictionary for Aspell'
echo

# Remove existing configuration
rm -f ~/.aspell.en.pws 2>/dev/null

ln -sv "$DOTFILES_DIR/aspell.en.pws" ~/.aspell.en.pws

echo

# }}}

# Enable startup system services {{{

echo 'Enable Startup system Services'
echo

for service in $DOTFILES_DIR/startup/*.service; do
  echo " - `basename "${service%.*}"`"

  systemctl --user enable $service >/dev/null 2>&1
done

systemctl --user daemon-reload >/dev/null

# }}}

# Npm configuration {{{

echo 'Setup Npm Configuration'
echo

# Remove existing configuration
rm -f ~/.npmrc 2>/dev/null

ln -sv "$DOTFILES_DIR/npmrc" ~/.npmrc

echo

# }}}

# MyCli configuration {{{

echo 'Setup MyCli Configuration'
echo

# Remove existing configuration
rm -f ~/.myclirc 2>/dev/null

ln -sv "$DOTFILES_DIR/myclirc" ~/.myclirc

echo

# }}}

# EditorConfig {{{

echo 'Setup EditorConfig'
echo

# Remove existing configuration
rm -f ~/.editorconfig 2>/dev/null

ln -sv "$DOTFILES_DIR/editorconfig" ~/.editorconfig

echo

# }}}

# rtv (reddit client) configuration {{{

echo 'Setup rtv (reddit client) Configuration'
echo

# Remove existing configuration
rm -f ~/.config/rtv/rtv.cfg 2>/dev/null

mkdir -p ~/.config/rtv >/dev/null 2>&1
ln -sv "$DOTFILES_DIR/rtv.cfg" ~/.config/rtv/rtv.cfg

echo

# }}}

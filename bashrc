#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias cdd="cd $DOWNLOADSDIR >/dev/null"
alias ffmpeg='ffmpeg -hide_banner'
alias gg='gitk --all --date-order'
alias g='git'
alias grep='grep --color=auto'
alias ls='ls -alh --color=auto --group-directories-first'
alias n='pnpm'
alias npm='pnpm'
alias root='sudo su -ls /usr/bin/bash'
alias tree='tree -C -a --dirsfirst -I "__pyc*|node_modules|elm-stuff|.git|target|.cache|autom4te.cache"'
alias veracrypt='veracrypt -t'
alias v='gvim'
alias yay='yay --removemake --combinedupgrade'

# Set SSH to use gpg-agent
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
  export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
fi

# Frequently used directories
export DOWNLOADSDIR="$HOME/Downloads"
export PROJECTSDIR="$HOME/projects"
export PYVEDIR="$HOME/opt/virtualenv"

PROMPT_COMMAND=set_cli_prompt

export EDITOR=vim
export TERM=xterm-256color

# History
export HISTFILESIZE=999999
export HISTSIZE=999999
export NODE_REPL_HISTORY_FILE=$HOME/.node_history
export NODE_REPL_HISTORY_SIZE=999999

# iBus settings
export GTK_IM_MODULE=ibus
export QT_IM_MODULE=ibus
export XIM_PROGRAM=/usr/bin/ibus-daemon
export XMODIFIERS=@im=ibus

# Less Colors for Man Pages
export LESS_TERMCAP_mb=$'\E[01;31m'       # begin blinking
export LESS_TERMCAP_md=$'\E[01;38;5;74m'  # begin bold
export LESS_TERMCAP_me=$'\E[0m'           # end mode
export LESS_TERMCAP_se=$'\E[0m'           # end standout-mode
export LESS_TERMCAP_so=$'\E[38;5;246m'    # begin standout-mode - info box
export LESS_TERMCAP_ue=$'\E[0m'           # end underline
export LESS_TERMCAP_us=$'\E[04;38;5;146m' # begin underline

# Generate aliases for screen profiles
for profile in `find $HOME/.profiles/ -maxdepth 1 -type d -regex '.+/[^\.]+$' -printf '%f\n'`;
do
    alias "$profile"="_scr $profile"
done

# Invoke GNU screen using extended configuration file
# Based on article by Joseph McCullough
# http://vertstudios.com/blog/multiple-screenrc-configurations-gnu-screen-tutorial/
function _scr() {
    if [[ -z $1 || ! -d $HOME/.profiles/$1 ]]; then
        echo "'$1' is not a valid profile"
        return 1
    else
        if [ -f $HOME/.profiles/$1/workspace.json ]; then
            WS_DEV='7: development'
            i3-msg "move container to workspace $WS_DEV; workspace $WS_DEV; append_layout $HOME/.profiles/$1/workspace.json;" >/dev/null
        fi

        screen -c $HOME/.profiles/$1/screenrc >/dev/null
    fi
}

# Start web server with current directory as web root
function httpd () {
    CADDYFILE="/tmp/Caddyfile-$$"
    ROOTDIR=$(pwd)
    HOST=$1

    if [ -z $HOST ]; then
        HOST="127.0.0.1"
    fi

    cat > $CADDYFILE<<EOF
$HOST:5000
log stdout
errors stderr

root $ROOTDIR

browse

header / {
  Cache-Control "no-store"
}
EOF

    caddy -conf $CADDYFILE
}

# Make directory in $1 if not exists and `cd` into it
function mcd() {
    DIR=$1

    if [ ! -d $DIR ]; then
        mkdir -p $DIR >/dev/null 2>&1

        if [ 0 -ne $? ]; then
            echo "Unable to create $DIR"
            exit 1
        fi
    fi

    cd $DIR
}

# Switch to the project working directory
function cdp() {
    DIR=$1

    if [ ! -d $PROJECTSDIR/$DIR ]; then
        echo "Directory not found: $PROJECTSDIR/$DIR";
        return 1;
    else
        cd $PROJECTSDIR/$DIR >/dev/null
    fi;
}

# Update CLI prompt
set_cli_prompt() {
    BG=236
    BG_INC=1

    # Ending
    P="\[\e[0;38;5;$(( BG + BG_INC ));48;5;${BG}m\]\[\e[0;38;5;${BG}m\]\[\e[0m\] "
    BG=$(( BG + BG_INC ))

    # Git branch
    GIT_BRANCH=`git status 2>/dev/null|head -n1|awk '{print $(NF)}'`
    if [ ! -z "$GIT_BRANCH" ]; then
        GIT_BLOCK="\[\e[0;38;5;$(( BG + BG_INC ));48;5;${BG}m\] \[\e[38;5;208m\]"
        GIT_TAG=`git describe --tags --abbrev=0 2>/dev/null`

        if [ ! -z "$GIT_TAG" ]; then
            GIT_BLOCK="$GIT_BLOCK\[\e[38;5;229m\] $GIT_TAG"
        fi

        #  = U+f126
        GIT_BLOCK="$GIT_BLOCK\[\e[38;5;249m\] $(_ellipsis "$GIT_BRANCH" 20 0)"
        BG=$(( BG + BG_INC ))

        # Working tree status
        GSW=$(git status -s 2>/dev/null | cut -c2 | uniq)

        # Index status
        GSI=$(git status -s 2>/dev/null | cut -c1 | uniq)

        STATUS_INDICATOR=''
        ICON_NEW=' '         # U+f055
        ICON_CHANGED_WD=' '  # U+f044
        ICON_CHANGED_IDX=' ' # U+f14b
        ICON_DELETED_WD=' '  # U+f2ed
        ICON_DELETED_IDX=' ' # U+f1f8
        ICON_CLEAN=' '       # U+f058
        ICON_UNTRACKED=' '   # U+f059
        ICON_MOVE=' '        # U+f079
        ICON_COPY=' '        # U+f0c5
        ICON_CONFLICT='💥'    # U+f14a5

        if [ ! -z "$GSW$GSI"  ]; then
            # Newly added to index
            if [[ $GSI =~ .*A.* ]]; then
              STATUS_INDICATOR="$STATUS_INDICATOR\[\e[38;5;2m\]$ICON_NEW"
            fi

            # Untracked file
            if [[ $GSW =~ .*\?.* ]]; then
                STATUS_INDICATOR="$STATUS_INDICATOR\[\e[38;5;$(( BG + BG_INC + BG_INC + BG_INC + BG_INC ))m\]$ICON_UNTRACKED"
            fi

            # Modified in index
            if [[ $GSI =~ .*M.* ]]; then
                STATUS_INDICATOR="$STATUS_INDICATOR\[\e[38;5;2m\]$ICON_CHANGED_IDX"
            fi

            # Modified in working tree
            if [[ $GSW =~ .*M.* ]]; then
                STATUS_INDICATOR="$STATUS_INDICATOR\[\e[38;5;3m\]$ICON_CHANGED_WD"
            fi

            # Deleted in index
            if [[ $GSI =~ .*D.* ]]; then
                STATUS_INDICATOR="$STATUS_INDICATOR\[\e[38;5;9m\]$ICON_DELETED_IDX"
            fi

            # Deleted from working tree
            if [[ $GSW =~ .*D.* ]]; then
                STATUS_INDICATOR="$STATUS_INDICATOR\[\e[38;5;1m\]$ICON_DELETED_WD"
            fi

            # Renamed in index
            if [[ $GSI =~ .*R.* ]]; then
                STATUS_INDICATOR="$STATUS_INDICATOR\[\e[38;5;13m\]$ICON_MOVE"
            fi

            # Copied in index
            if [[ $GSI =~ .*C.* ]]; then
                STATUS_INDICATOR="$STATUS_INDICATOR\[\e[38;5;2m\]$ICON_COPY"
            fi

            # Merge conflicts in index or working tree
            if [[ "$GSW$GSI" =~ .*U.* ]]; then
                STATUS_INDICATOR="$STATUS_INDICATOR\[\e[38;5;1m\]$ICON_CONFLICT"
            fi
        else
            STATUS_INDICATOR="$STATUS_INDICATOR\[\e[38;5;10m\]$ICON_CLEAN"
        fi

        P="$GIT_BLOCK $STATUS_INDICATOR$P"
    fi

    # Python virtual environment
    if [ ! -z "$VIRTUAL_ENV" ]; then
        P="\[\e[0;38;5;$(( BG + BG_INC ));48;5;${BG}m\]\[\e[38;5;45m\] `basename $VIRTUAL_ENV` $P"
        BG=$(( BG + BG_INC ))
    fi

    # Working directory
    P="\[\e[0;48;5;${BG}m\]\[\e[38;5;249m\] $(_prompt_dir)$P"

    PS1=$P
}

_prompt_dir() {
  # $HOME directory
  if [ "$PWD" = "$HOME" ]; then
    # show home icon (U+F015)
    echo -n "\[\e[38;5;140m\] "

    return

  # Projects directory
  elif [[ "$PWD" =~ ^$HOME\/projects.* ]]; then
    # show open folder icon (U+F07C) in bright green(ish) color
    echo -n "\[\e[38;5;40m\]  "

  # Scripts directory
  elif [[ "$PWD" =~ ^$HOME\/scripts.* ]]; then
    # show open folder icon (U+F07C) in dark green(ish) color
    echo -n "\[\e[38;5;36m\]  "

  # Directories with sensitive information
  elif [[ "$PWD" =~ ^$HOME\/(vault|\.ssh|\.gnupg).* ]]; then
    # show open folder icon (U+F07C) in red(ish) color
    echo -n "\[\e[38;5;202m\]  "

  # Downloads directory
  elif [[ "$PWD" =~ ^$HOME\/Downloads.* ]]; then
    # show download icon (U+F019)
    echo -n "\[\e[38;5;192m\]  "

  # /tmp directory
  elif [[ "$PWD" =~ ^\/tmp.* ]]; then
    # show temporary (recycle) icon (U+F1B8)
    echo -n "\[\e[38;5;214m\]  "

  # Any other directory
  else
    # show open folder icon (U+F07C) in blue(ish) color
    echo -n "\[\e[38;5;39m\]  "
  fi

  # Include directory name
  echo -n "\[\e[38;5;249m\]$(_ellipsis "$(basename "$PWD")" 12 0)"
}

_ellipsis() {
  local TEXT=$1
  local LN_LHS=${2:-20}
  local LN_RHS=${3:-10}
  local LEN=${#TEXT}
  local MAX_LEN=$(( LN_RHS + LN_LHS + 1))

  if [ $LEN -le $MAX_LEN ]; then
    echo -n $TEXT
  else
    # ellipsis (U+2026)
    echo "${TEXT:0:LN_LHS}…${TEXT:$(( LEN - LN_RHS ))}"
  fi
}

[[ -f ~/.bashrc.local ]] && . ~/.bashrc.local
